#!/bin/bash
test="HLT"
usage="Usage: $0 (CoralAccess|CoolRegression|HLT) (stdout|fileProxy0|fileProxy) (LCGCMT_xx|cwd)"
if [ "$3" == "" ] || [ "$4" != "" ]; then
  echo "$usage"; exit 1
fi

test=$1
cout=$2
lcg=$3
shift
shift
shift

pushd `dirname $0` > /dev/null
dir=`pwd`

if [ "$test" == "CoralAccess" ]; then
  baselog=proxyLog
elif [ "$test" == "CoolRegression" ]; then
  baselog=coolProxyLog
elif [ "$test" == "HLT" ]; then
  baselog=hltProxyLog
else
  echo "$usage"; exit 1
fi
basedir=`cd $dir/../../..; pwd`/logs/CORAL_SERVER/$test

if [ "$cout" == "stdout" ]; then
  outfile=/dev/stdout
  echo "Proxy  output to stdout:" $outfile
else
  if [ "$cout" == "fileServer" ]; then
    filename=${baselog}_server
  elif [ "$cout" == "fileProxy0" ]; then
    filename=${baselog}_proxy0
  elif [ "$cout" == "fileProxy" ]; then
    filename=${baselog}_proxy
  else
    echo "$usage"; exit 1
  fi
  outfile=${basedir}/proxy/${filename}.txt
  echo "Proxy  output file: " $outfile
fi

if [ "$lcg" == "LCGCMT_${lcg/#LCGCMT_}" ]; then
  lcgseries=${lcg:7:2}
  if [[ $lcgseries < 68 ]]; then
    lcgconf=/afs/cern.ch/sw/lcg/app/releases/LCGCMT/$lcg/LCG_Configuration/cmt/requirements
    release=/afs/cern.ch/sw/lcg/app/releases/CORAL/`grep CORAL $lcgconf | awk '{print substr($3,2,length($3)-2)}'`
  else
    lcgconf=/afs/cern.ch/sw/lcg/releases/LCGCMT/$lcg/LCG_Configuration/cmt/requirements
    release=/afs/cern.ch/sw/lcg/releases/${lcg/LCGCMT_/LCG_}/CORAL/`grep CORAL $lcgconf | awk '{print substr($3,2,length($3)-2)}'`
  fi
  if [[ $lcgseries < 81 ]]; then # Check no longer needed with cmake
    if [ ! -f "$lcgconf" ]; then
      echo "ERROR! $lcgconf does not exist"
      exit 1
    fi
  fi
  if [[ "$lcgseries" < 59 ]]; then
    echo "ERROR! Only LCGCMT releases >= 59 are supported"
    exit 1
  fi
elif [ "$lcg" == "cwd" ]; then
  release=`cd ../../..; pwd`
else
  echo "$usage"; exit 1
fi

echo `date +'%a %b %d %H:%M:%S.%N %Z %Y'` on `hostname` > $outfile
echo "Kill any running instance of coralServerProxy and sleep 1s" >> $outfile
killall -9 coralServerProxy >& /dev/null
sleep 1

# Reset CMTCONFIG for HLT tests from an installed released (not 'cwd')
if [ "$test" == "HLT" ] && [ "$lcg" == "LCGCMT_${lcg/#LCGCMT_}" ]; then
  if [[ $lcgseries < 68 ]]; then
    if [ "$CMTCONFIG" != "i686-slc5-gcc43-dbg" ] && [ "$CMTCONFIG" != "i686-slc5-gcc43-opt" ]; then export CMTCONFIG=i686-slc5-gcc43-dbg; fi
  elif [[ $lcgseries < 81 ]]; then
    export CMTCONFIG=x86_64-slc6-gcc48-opt
  else
    unset CMTCONFIG
    export BINARY_TAG=x86_64-slc6-gcc49-opt
  fi
fi

if [[ $lcgseries < 81 ]] && [ "$lcg" == "LCGCMT_${lcg/#LCGCMT_}" ]; then
  # Setup CMT and environment from src/config/cmt
  pushd $release/src/config/cmt > /dev/null
  export SITEROOT=/afs/cern.ch # workaround for bug #87325 in older releases
  echo "Setup CMT environment using CMT_env.sh" >> $outfile
  if [ ! -f CMT_env.sh ]; then
    echo "ERROR! No CMT_env.sh in `pwd`" >> $outfile
    if [ "$cout" != "stdout" ]; then echo "ERROR! No CMT_env.sh in `pwd`"; fi
    exit 1
  fi 
  . CMT_env.sh >> $outfile
  if [ ! -f setup.sh ]; then
    echo "WARNING! No setup.sh in `pwd`" >> $outfile
    if [ $cout != "stdout" ]; then echo "WARNING! No setup.sh in `pwd`"; fi
    tempfile=`mktemp /tmp/tmp.$USER.XXXXXXXXXX`
    ${CMTROOT}/mgr/cmt setup -sh -pack=config -version=v1 -path=$release/src -no_cleanup $* > ${tempfile}
    echo "Set up environment from ${tempfile} instead"
    . ${tempfile} > /dev/null
    ###/bin/rm -f ${tempfile}
  else
    . setup.sh > /dev/null
  fi 
  popd > /dev/null
  ccrun=
else
  # Setup using cmake
  release=$release/$BINARY_TAG
  ccrun=$release/cc-run
  if [ ! -f $ccrun ]; then
    echo "ERROR! $ccrun does not exist"
    exit 1
  fi
fi

unset CORAL_AUTH_PATH
unset CORAL_DBLOOKUP_PATH

unset CORALSERVER_DEBUG
unset CORALSTUBS_DEBUG
unset CORALSOCKETS_DEBUG
#export CORALSERVER_DEBUG=1
#export CORALSTUBS_DEBUG=1
#export CORALSOCKETS_DEBUG=1

msglevel=""       # info (default)
###msglevel="-vv" # verbose

echo `date +'%a %b %d %H:%M:%S.%N %Z %Y'` on `hostname` >> $outfile
if [[ $lcgseries < 81 ]] && [ "$lcg" == "LCGCMT_${lcg/#LCGCMT_}" ]; then
  pushd $release/src/config/cmt > /dev/null
  echo "CMTCONFIG is $CMTCONFIG" >> $outfile
  echo "Using LCG:    "`cmt show macro_value LCG_config_version` >> $outfile
  echo "Using LCGCMT: "`cmt show macro_value LCGCMT_home` >> $outfile
  echo "Using Boost:  "`cmt show macro_value Boost_home` >> $outfile
  echo "Using CORAL:  "`cmt show macro_value CORAL_home` >> $outfile
  echo "Using ROOT:   "`cmt show macro_value ROOT_home` >> $outfile
  echo "Using COOL:   "`cmt show macro_value COOL_home` >> $outfile
  popd > /dev/null
else
  echo "BINARY_TAG is $BINARY_TAG" >> $outfile  
  echo "Using CORAL: "`$ccrun printenv CORALSYS` >> $outfile  
fi
echo "*******************************************************************" >> $outfile
$ccrun /usr/bin/time -ao/dev/stdout -f"=== TIMER (SPROXY) %Uuser %Ssystem %eelapsed %PCPU" coralServerProxy -m -p 40008 -s 40007 0.0.0.0 $msglevel >> $outfile 2>&1
echo "*******************************************************************" >> $outfile 
echo `date +'%a %b %d %H:%M:%S.%N %Z %Y'` on `hostname` >> $outfile

if [ "$cout" != "stdout" ]; then
  # Remove CVS reserved words from the logfiles
  \mv ${outfile} ${outfile}.old
  cat ${outfile}.old | sed 's/\$''Name/$CvsName/' | sed 's/\$''Id/$CvsId/' > ${outfile}
  \rm ${outfile}.old
fi

if [ "$cout" != "stdout" ]; then
  \mv ${outfile} ${outfile}.old
  ./sedDate.sh ${outfile}.old > ${outfile}
  ###\rm ${outfile}.old
fi

popd > /dev/null
